execute pathogen#infect()
call pathogen#helptags()
syntax on
filetype plugin indent on
set tabstop=2
set shiftwidth=2
set softtabstop=2
set backspace=indent,eol,start
set expandtab
colo jellybeans
set hlsearch
set incsearch
set number
set noerrorbells visualbell
let mapleader = "\<Space>"
autocmd BufNewFile,BufRead *.json set ft=javascript
autocmd BufNewFile,BufRead *.avsc set ft=javascript
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
set clipboard^=unnamedplus,unnamed
set nocompatible
let g:CommandTWildIgnore=&wildignore . ",*node_modules*"